.PHONY: clean
#调试时使用-g标签
CFLAGS= -c -g
# releases 发行
3500dict: ./build/config.o ./build/wcharlib.o ./build/main.o
	gcc $^ -o $@

./build/%.o: %.c 3500.h
	gcc ${CFLAGS} $< -o $@

clean:
	rm -r ./build/*

# tests 单文件测试
test:config.c
	gcc -g config.c -o test
	./test
test2:wcharlib.c
	gcc -g wcharlib.c -o test2
	./test2