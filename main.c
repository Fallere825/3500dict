#include "3500.h"
#include <locale.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <wchar.h>

tree dict;

int main(int argc, char *argv[]) {
    setlocale(LC_ALL, "zh_CN.UTF-8");
    //读入
    // printf("path to file:");
    // char path[100];
    // path = argv[2];
    FILE *dict_file;
    dict_file = fopen(argv[1], "r");
    if (dict_file == NULL) return ERR;
    readfile(dict_file, &dict);
    
    while (dict.sum > 0) {
        srand((unsigned)time(NULL));
        int num = rand() % dict.sum + 1; //1~dict.sum+1
        int n1 = 0;
        for (
            int i = 1; i < num;
            ++i) //不能取到最后一个点,因为实际是对第n1+1个进行操作(为方便移除节点)
            n1 = dict.elements[n1].next;
        int n = dict.elements[n1].next;
        while (true) {
            setlocale(LC_ALL, "zh_CN.UTF-8");
            wchar_t inword[100]; // inword=input word
            printf("%ls: ", dict.elements[n].self.key);
            readline_easy(inword);
            if (correct(inword, dict.elements[n].self.value)) {
                printf("\nCORRECT!\n");
                //如果有sub key/value
                int m = 1;
                while (wcscmp(dict.elements[n].sub[m].key, L"") != 0) {
                    wchar_t sub_inword[100]; // inword=input word

                    while (true) {
                        printf("%ls: ", dict.elements[n].sub[m].key);
                        readline_easy(sub_inword);
                        if (correct(sub_inword,
                                    dict.elements[n].sub[m].value)) {
                            printf("\nCORRECT!\n");
                            break;
                        }
                    }
                    m++;
                }
                break;
            }
        }
        dict.sum--;
        dict.elements[n1].next = dict.elements[n].next;
    }
    return 0;
}