/*----------------------------------------
Copyright 2024 ZhangYanHao

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
----------------------------------------*/
#ifndef HEADER
#define HEADER
#include <bits/types/FILE.h>
#include <stdbool.h>
#include <wchar.h>

struct kv {
    wchar_t key[100];
    wchar_t value[100];
};
struct atom {
    struct kv self;
    struct kv sub[30];
    int next;
};
typedef struct {
    struct atom elements[100];
    int sum;
} tree;

typedef int STATUS;
#define ERR (-1)
#define OK (0)

extern STATUS readfile(FILE* file, tree *ct);
extern bool correct(wchar_t str1[], wchar_t str2[]);
extern void readline_easy(wchar_t str1[]);
#endif