/*此文件实现基本的文件读写*/
#include "3500.h"
#include <stdbool.h>
#include <stdio.h>
#include <wchar.h>
#include <locale.h>

// #define DEBUG
STATUS readfile(FILE* file, tree *ct) { // ct=config tree
    setlocale(LC_ALL, "zh_CN.UTF-8");
    //读取
    wchar_t buff;
    int ret;
    int i = 1, subi = 0;
    int j = 0, subj = 0;
    bool iskey = true;  //当前读得是否是key (is key)
    bool issub = false; // is sub key/value
    do {
        ret = fwscanf(file, L"%lc", &buff);
        if (buff == '\n') {
            continue;
        } else if (buff == ':') {
            // ct->elements[i].self.key[j] = '\0';
            // ct->elements[i].sub[subi].key[subj]='\0';
            iskey = false;
            j = 0;
            subj = 0;
        } else if (buff == ';') {
            // ct->elements[i].self.value[j] = '\0';
            // ct->elements[i].sub[subi].value[subj] = '\0';
            iskey = true;
            issub = false;
            i++;
            j = 0; 
            subj = 0;
        } else if (buff == '\t') {
            issub = true;
            i--;
            subi++;
        } else {
            if (issub) {
                if (iskey) ct->elements[i].sub[subi].key[subj] = buff;
                if (!iskey) ct->elements[i].sub[subi].value[subj] = buff;
                subj++;
            } else {
                if (iskey) ct->elements[i].self.key[j] = buff;
                if (!iskey) ct->elements[i].self.value[j] = buff;
                j++;
            }
        }
    } while (ret != EOF);

    //填入初始化信息
    ct->sum = 1;
    while (wcscmp(ct->elements[ct->sum].self.key, L"") != 0) {
        ct->elements[ct->sum - 1].next = ct->sum;
        ct->sum++;
    }
    ct->sum = ct->sum - 1;

    //关闭文件
    fclose(file);
    return OK;
}

#ifdef DEBUG
int main() {
    tree testT;
    STATUS st1 = readfile("dict2.txt", &testT);
    printf("\nbye %d\n", testT.sum);
    return 0;
}
#endif