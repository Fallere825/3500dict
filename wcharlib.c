#include "3500.h"
#include <stdbool.h>
#include <stdio.h>
#include <wchar.h>
// #define DEBUG

//忽略空格和大小写比较两个字符串
bool correct(wchar_t str1[], wchar_t str2[]) {
    int i = 0, j = 0;
    while (str1[i] != L'\000') {
        wchar_t buff1, buff2;
        while (str1[i] == ' ') i++;
        while (str2[j] == ' ') j++;
        if (str1[i] != str2[j] && str1[i] + 32 != str2[j] &&
            str1[i] - 32 != str2[j]) {
            return false;
        }
        i++;
        j++;
    }
    return true;
}

//直接读取一整行数据
void readline_easy(wchar_t str1[]) {
    int i = 0;
    wchar_t buff;
    do {
        buff = getwchar();
        str1[i++] = buff;
    } while (buff != L'\n');
    str1[--i]=L'\0';
}
#ifdef DEBUG
int main() {
    // wchar_t s1[] = L"nai";
    // wchar_t s2[] = L"NA ";
    // // wprintf(L"%ld %ld\n", s1[1], s2[1]);
    // bool a = correct(s1, s2);
    // printf("%d", a);
    // return 0;
    wchar_t a[100];
    readline_easy(a);
    wprintf(L"%ls", a);
}
#endif